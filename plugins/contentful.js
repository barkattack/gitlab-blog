import { createClient } from 'contentful';

const isContentPreview = process.env.CTF_LIVE_PREVIEW === 'active' || process.env.CI_COMMIT_BRANCH !== process.env.CI_DEFAULT_BRANCH

const host = isContentPreview ? 'preview.contentful.com' : 'cdn.contentful.com';

let config = {
  space: process.env.CTF_SPACE_ID,
  accessToken: isContentPreview
    ? process.env.CTF_CONTENT_PREVIEW_ACCESS_TOKEN
    : process.env.CTF_CDA_ACCESS_TOKEN,
  host,
  environment: process.env.CTF_ENVIRONMENT || 'master',
  retryLimit: 1000,
  logHandler: (level, data) => {
    if (data.includes('Rate limit')) {
      return;
    }
  }
};

const navigationConfig = {
  space: process.env.CTF_NAV_SPACE_ID,
  accessToken: isContentPreview
      ? process.env.CTF_NAV_PREVIEW_ACCESS_TOKEN
      : process.env.CTF_NAV_CDA_ACCESS_TOKEN,
  host,
  retryLimit: 1000,
  logHandler: (level, data) => {
    if (data.includes('Rate limit')) {
      return;
    }
  }
};

let client;
let navClient;

export const getClient = (configOverride) => {

  if(configOverride){
    config = {...config, ...configOverride}
  }
  if (!client) {
    client = createClient(config);
  }

  return client;
};

export const getNavigationClient = () => {
  if (!navClient) {
    navClient = createClient(navigationConfig);
  }

  return navClient;
};
