import { ContentfulLivePreview } from '@contentful/live-preview';

ContentfulLivePreview.init({ locale: 'en-US'});