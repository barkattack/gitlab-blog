import Vue from 'vue'
import {post} from '../interfaces'

export const ParseUrlMixin = Vue.extend({
  methods:{
    parseUrl(post: post){
      if(!post || !post.fields.date){
        return ''
      }
      return post.fields.externalUrl || `/blog/${post.fields.date.replaceAll('-','/')}/${post.fields.slug}/`
    }
  }
})