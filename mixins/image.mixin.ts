import Vue from 'vue'

export const ImageUrlMixin = Vue.extend({
  methods: {
    parseImageUrl(imageUrl: string, width: string | number, height: string | number, format: string = 'webp', fit: string = 'scale') {
      if (!imageUrl) {
        return ''
      }
      return `${imageUrl}?fm=${format}${width ? `&w=${width}` : ''}${height ? `&h=${height}` : ''}`
    }
  }
})