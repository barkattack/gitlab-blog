import {
  fetchPostRoutes,
  fetchAuthorsRoutes,
  fetchCategoryRoutes,
  fetchTagRoutes,
} from "./fetchContentfulRoutes";

export default function () {
  this.nuxt.hook("generate:done", async (context) => {
    const postRoutes = await fetchPostRoutes();
    const authorRoutes = await fetchAuthorsRoutes();
    const categoryRoutes = await fetchCategoryRoutes();
    const tagRoutes = await fetchTagRoutes();

    const allRoutes = [
      ...postRoutes,
      ...authorRoutes,
      ...categoryRoutes,
      ...tagRoutes,
    ];

    if (allRoutes.length === 0) {
      throw new Error(
        "Sitemap generation error: slugs are not being correctly returned"
      );
    }

    const exclude = this.nuxt.options.sitemap.exclude;

    // Remove routes listed in the 'exclude' array from the sitemap module
    const filteredRoutes = allRoutes.filter((route) => {
      return !exclude.some((excludedPattern) =>
        route.includes(excludedPattern)
      );
    });

    this.nuxt.options.sitemap.routes = [...filteredRoutes];
  });
}
