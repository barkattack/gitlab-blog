import { getClient } from "../plugins/contentful";
import { CONTENT_TYPES } from "../contentful-types.js";
import { CONFIDENTIAL_TAG } from "../common/constants.ts";

import markdownIt from "markdown-it";

const md = markdownIt();

function markdownToHtml(markdown) {
  return md.render(markdown);
}

const generateUrlPath = (dateString, slug) => {
  const date = new Date(Date.parse(dateString));
  const year = date.getFullYear().toString().padStart(4, "0");
  const month = (date.getMonth() + 1).toString().padStart(2, "0");
  const day = date.getDate().toString().padStart(2, "0");
  const urlPath = `/${year}/${month}/${day}/${slug}`;

  return urlPath;
};

export function generateFeed() {
  return [
    {
      path: "/atom.xml",
      async create(feed) {
        const entries = await getClient().getEntries({
          content_type: CONTENT_TYPES.BLOG_POST,
          'metadata.tags.sys.id[nin]': CONFIDENTIAL_TAG,
          'fields.externalUrl[exists]':false,
          order: "-fields.date",
          skip: 0,
          limit: 20,
        });

        const posts = entries.items;

        feed.options = {
          title: "GitLab",
          id: "https://about.gitlab.com/blog",
          link: "https://about.gitlab.com/blog/",
          updated: new Date(posts[0].fields.date),
          author: {
            name: "The GitLab Team",
          },
        };

        posts.forEach((post) => {
          const path = generateUrlPath(post.fields.date, post.fields.slug);

          const articleDate = post.fields.date;
          const formattedDate = new Date(articleDate);
          const formattedBody = markdownToHtml(post.fields.body);

          feed.addItem({
            title: post.fields.title,
            link: `https://about.gitlab.com/blog${path}`,
            id: `https://about.gitlab.com/blog${path}`,
            published: formattedDate,
            date: formattedDate,
            author: post.fields.authors?.map(author=>author.fields.name).join(', '),
            content: formattedBody,
          });
        });
      },
      type: "atom1",
    },
  ];
}
