const contentful = require('contentful-management')
const process = require('process');
const accessToken = process.argv[2] // CMA access token
const spaceId = process.arg[3] // Blog space id

function publishAllDraftEntries(auth, page = 1) {

  const client = contentful.createClient({
    accessToken: auth.accessToken 
  })

  client.getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env)).then(environment => {

      environment.getEntries({ limit: 1000, skip: (page - 1 ) * 1000  }).then(async ({ items }) => {
        console.log(items.length)
        for (let entry of items) {
          if (entry.isDraft()) {
            entry.publish().then(() => console.log('entry published'))
            await delay(150)
          }
        }
      })

      // environment.getAssets({ limit: 1000, skip: (page - 1 ) * 1000 }).then(async ({ items }) => {
      //   console.log(items.length)
      //   for (let asset of items) {
      //     if (asset.isDraft()) {
      //       asset.publish().then(() => console.log('asset published')).catch(error => console.log(error))
      //       await delay(150)
      //     }
      //   }
      // })
    })
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

publishAllDraftEntries({
  accessToken,
  spaceId,
  env: 'master',
}, 3)
