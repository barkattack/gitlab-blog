const yaml = require('js-yaml');
const fs = require('fs');
const contentful = require('contentful-management')
const process = require('process');
const blogFiles = '/Users/mateofernandopenagos/Documents/gitlab/www-gitlab-com/sites/uncategorized/source/blog/blog-posts' // blog files folder in www to use as migration target
const authorFilesFolder = '/Users/mateofernandopenagos/Documents/gitlab/www-gitlab-com/data/team_members/person' // author files folder in www to use in the migration
const accessToken = process.argv[2] // CMA access token
const spaceId = process.argv[3] // Blog space id



async function migrateBlog(auth, blogFilePath, authorFilePath, startFrom) {

  const client = contentful.createClient({
    accessToken: auth.accessToken
  })
  const environment = await client.getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env))
  let { parsedPosts, parsedAuthorFiles } = parseToJSON(blogFilePath, authorFilePath)
  if(startFrom){
    const startIndex = parsedPosts.findIndex(post => post.title === startFrom)
    console.log('starting from post at index ', startIndex)
    parsedPosts = parsedPosts.splice(startIndex, parsedPosts.length -1)
  }
  for (const [index, post] of parsedPosts.entries()) {
    //  Create required entries for the blog post:
    //  Asset
    const assetId = await createPostImage(post, environment);

    //  Author
    //    Author headshot asset
    const authorId = await createAuthor(post, parsedAuthorFiles, environment)
    //  category
    //  Check if category already exists.
    const categoryId = await createCategory(post, environment)
    //  Create blogpost 
    await createBlogPost(post, assetId, authorId, categoryId, environment)
    console.log(`PROGRESS: ${Math.round(index / parsedPosts.length * 100)}%`)
  }


}

function parseToJSON(blogContentPath, authorFilesPath) {
  console.log('parsing blog files to JSON')
  const blogPosts = fs.readdirSync(blogContentPath);
  const parsedPosts = blogPosts.map(file => {
    const [__, yml, markdown] = fs.readFileSync(`${blogContentPath}/${file}`, { encoding: 'utf8', flag: 'r' }).split('---')
    const parsedYML = yaml.load(yml, { json: true })
    // Make relative images absolute since the markdown references assets that are not in the CMS
    const updatedMarkdown = makeImagePathsAbsolute(markdown)
    return { ...parsedYML, path: file, markdown: updatedMarkdown }
  }).filter(file => file)

  console.log(`Successfully parsed ${parsedPosts.length} blog files`)
  console.log('Parsing author files to retrieve author images')

  const authorFiles = getFileList(authorFilesPath);
  const parsedAuthorFiles = authorFiles.map((file) => {
    try {
      yml = fs.readFileSync(file, { encoding: 'utf8', flag: 'r' })
      return parsedYML = yaml.load(yml, { json: true })
    } catch (error) {
      return {}
    }
  }).filter(file => file)

  console.log(`Successfully parsed ${parsedAuthorFiles.length} author files`)

  return { parsedPosts, parsedAuthorFiles };
}

async function createBlogPost(post, assetId, authorId, categoryId, environment) {
  const contentType = 'blogPost'
  const [year, month, day] = post.path.split('-')
  const date = `${year}-${month}-${day}`
  let id = `${date}-${post.title}`
  id = id.replace(/[^a-zA-Z0-9 ]/g, '').replaceAll(' ', '-').substring(0, 55)
  console.log('creating post')
  console.log(assetId, authorId, categoryId, id)
  if (!assetId || !authorId) {
    console.log('invalid post data, skipping')
    return;
  }
  try {
    const [slugWithDate] = post.path.split('.')
    const [year, month, day, ...slugArray] = slugWithDate.split('-');
    const slug = post.canonical_path || slugArray.join('-');
    const newPost = await environment.createEntryWithId(contentType, id, {
      fields: {
        title: {
          'en-US': post.title
        },
        slug: {
          'en-US': slug
        },
        previewSlug:{
          'en-US': `${year}/${month}/${day}/${slug}`
        },
        date: {
          'en-US': date
        },
        body: {
          'en-US': post.markdown
        },
        description: {
          'en-US': post.description
        },
        featured: {
          'en-US': false
        },
        tags: {
          'en-US': post.tags ? post.tags.split(',') : []
        },
        image: {
          'en-US': {

            sys: {
              type: "Link",
              linkType: "Asset",
              id: assetId
            }
          }
        },
        author: {
          'en-US': {

            sys: {
              type: "Link",
              linkType: "Entry",
              id: authorId
            }
          }
        },
        category: {
          'en-US': {

            sys: {
              type: "Link",
              linkType: "Entry",
              id: categoryId
            }
          }
        }

      }
    })
    console.log('Created post')
    return id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message)
    if (parsedError.status == 409) {
      console.log('Post already exists, skipping')
    } else {
      console.log(parsedError)
      console.log(parsedError.message)
    }
    return id
  }
}

async function createCategory(post, environment) {
  const contentType = 'category'
  const id = post.categories.replace(/[^a-zA-Z0-9 ]/g, '').replaceAll(' ', '-');
  console.log(`creating category ${id}`)
  try {
    const category = await environment.createEntryWithId(contentType, id, {
      fields: {
        name: {
          'en-US': post.categories
        },
      }
    })
    console.log('Created category')
    return id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message)
    if (parsedError.status == 409) {
      console.log('Category already exists, skipping')
      return id
    } else {
      console.log(parsedError.message)
    }
  }
}

async function createAuthor(post, authorArray, environment) {
  if (!post.author) {
    console.log('post has no author')
    return
  }
  const contentType = 'author'
  // Fallback image in case an author doesn't have an image
  let authorImageID = '5ZSyWACGhAwA9IleY5vdkS'
  if (post.author_gitlab) {
    // Search for author image and create asset
    console.log(`creating headshot for ${post.author_gitlab}`)
    authorImageID = await createAuthorHeadshot(post.author_gitlab, authorArray, environment)
  }
  let id = post.author_gitlab || post.author
  id = id.replace(/[^a-zA-Z0-9 ]/g, '').replaceAll(' ', '-')
  console.log(`creating author ${id}`)
  try {
    const author = await environment.createEntryWithId(contentType, id, {
      fields: {
        name: {
          'en-US': post.author
        },
        gitLabHandle: {
          'en-US': post.author_gitlab
        },
        headshot: {
          'en-US': {

            sys: {
              type: "Link",
              linkType: "Asset",
              id: authorImageID
            }
          }
        }
      }
    })
    console.log('Created author')
    return id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message)
    if (parsedError.status == 409) {
      console.log('Author already exists, skipping')
      return id
    } else {
      console.log(parsedError.message)
    }
  }

}

async function createAuthorHeadshot(gitlabHandle, authors, environment) {
  const authorFile = authors.find(author => author.gitlab === gitlabHandle)
  if (authorFile) {
    authorImagePath = `/Users/mateofernandopenagos/Documents/gitlab/www-gitlab-com/sites/uncategorized/source/images/team/${authorFile.picture}`
    console.log('Author file found, creating asset')
    if (authorFile.picture.includes('gitlab-logo-extra-whitespace') || !authorFile.picture || !fs.existsSync(authorImagePath)) {
      console.log('Author is using the fallback image, skipping creation')
      return '5ZSyWACGhAwA9IleY5vdkS'
    }
    const imageTitle = `${authorFile.gitlab}-headshot`
    const readStream = fs.createReadStream(authorImagePath)
    try {
      const asset = await environment.createAssetFromFiles({
        fields: {
          title: {
            'en-US': imageTitle
          },
          description: {
            'en-US': `${authorFile.name}'s headshot image`
          },
          file: {
            'en-US': {
              contentType: 'image/jpeg',
              fileName: imageTitle,
              file: readStream
            }
          }
        },
        sys: {
          'en-Us': {
            id: authorFile.picture
          }
        }
      })
      console.log('Created asset, processing')
      asset.processForAllLocales();
      return asset.sys.id;
    } catch ({ name, message }) {
      const parsedError = JSON.parse(message)
      if (parsedError.status == 409) {
        console.log('Asset already exists, skipping')
        return imageTitle
      } else {
        console.log(parsedError.message)
      }
    }
  }
}

async function createPostImage(post, environment) {
  if (!post.image_title) {
    console.log('Post has no image')
    // Fallback hero image
    return '21mOmoq0dnepgpLzqI703C';
  }
  console.log(`Creating new asset`)
  // Creating assets with ID to prevent asset duplication during migration
  const imageTitle = post.image_title.split('/').pop()
  try {

    const asset = await environment.createAssetWithId(imageTitle, {
      fields: {
        title: {
          'en-US': imageTitle
        },
        description: {
          'en-US': `image to be used in the "${post.title}" post`
        },
        file: {
          'en-US': {
            contentType: 'image/jpeg',
            fileName: imageTitle,
            upload: post.image_title.includes('http')? post.image_title :`https://about.gitlab.com${post.image_title}`
          }
        }
      }
    })
    console.log('Asset created, processing')
    asset.processForAllLocales();
    return asset.sys.id;
  } catch ({ name, message }) {
    const parsedError = JSON.parse(message)
    if (parsedError.status == 409) {
      console.log('Asset already exists, skipping')
      return imageTitle
    } else {
      console.log(parsedError.message)
    }
  }
}

function slugify(string) {
  return string.toLowerCase()
    .trim()
    .replace(/[^\w\s-]/g, '')
    .replace(/[\s_-]+/g, '-')
    .replace(/^-+|-+$/g, '');
}

const getFileList = (dirName) => {
  let files = [];
  const items = fs.readdirSync(dirName, { withFileTypes: true });

  for (const item of items) {
    if (item.isDirectory()) {
      files = [...files, ...getFileList(`${dirName}/${item.name}`)];
    } else {
      files.push(`${dirName}/${item.name}`);
    }
  }

  return files;
};

function makeImagePathsAbsolute(markdownText) {
  let regex = /(!\[.*?\]\()(\/[^)]+)\)/g;

  let prefix = "https://about.gitlab.com";

  let newMarkdownText = markdownText.replace(regex, `$1${prefix}$2)`);


  return newMarkdownText
}


migrateBlog({
  accessToken,
  spaceId,
  env: 'master'
}, blogFiles, authorFilesFolder, 'Learning Python with a little help from AI');






