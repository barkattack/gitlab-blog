const contentful = require('contentful-management')
const process = require('process');
const accessToken = process.argv[2] // CMA access token
const spaceId = process.arg[3] // Blog space id


function deleteAllDraftEntries(auth) {

  const client = contentful.createClient({
    accessToken: auth.accessToken 
  })

  client.getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env)).then(environment => {

      environment.getEntries({ limit: 1000 }).then(async ({ items }) => {
        console.log(items.length)
        for (let entry of items) {
          if (entry.isDraft()) {
            entry.delete().then(() => console.log('asset deleted'))
          }
          await delay(150)
        }
      })

      environment.getAssets({ limit: 1000 }).then(async ({ items }) => {
        console.log(items.length)
        for (let entry of items) {
          if (entry.isDraft()) {
            entry.delete().then(() => console.log('asset deleted'))
          }
          await delay(150)
        }
      })
    })
}


function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

//  Used to clean all drafts from the space after a mass import if needed, careful not to run on accident!
deleteAllDraftEntries({
  accessToken,
  spaceId,
  env: 'master'
})
