const contentful = require('contentful-management')
const process = require('process');
const levenshtein = require('js-levenshtein')
const accessToken = process.argv[2] // CMA access token
const spaceId = process.argv[3] // Blog space id
const page = process.argv[4]

const validTags = [
  "agile",
  "AI/ML",
  "AWS",
  "bug bounty",
  "careers",
  "CI",
  "CD",
  "CI/CD",
  "cloud native",
  "code review",
  "collaboration",
  "community",
  "contributors",
  "customers",
  "demo",
  "design",
  "developer survey",
  "DevOps",
  "DevOps platform",
  "DevSecOps",
  "DevSecOps platform",
  "events",
  "features",
  "frontend",
  "Group Conversations",
  "git",
  "GitOps",
  "GKE",
  "google",
  "growth",
  "inside GitLab",
  "integrations",
  "kubernetes",
  "news",
  "open source",
  "partners",
  "patch releases",
  "performance",
  "product",
  "production",
  "releases",
  "remote work",
  "research",
  "security",
  "security releases",
  "security research",
  "solutions architecture",
  "startups",
  "testing",
  "tutorial",
  "UI",
  "user stories",
  "UX",
  "webcast",
  "workflow",
  "zero trust",
]

function fixBrokenTags(auth, page = 1) {
  const failedRequests = [];
  const client = contentful.createClient({
    accessToken: auth.accessToken

  })

  client.getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env)).then(environment => {

      environment.getEntries({ limit: 500, skip: (page - 1) * 500, content_type: 'blogPost'}).then(async ({ items }) => {
        console.log(items.length)
        for (let entry of items) {
          try{

            if ((entry.isPublished() || isChanged(entry)) && entry.fields.tags) {
              const tags = entry.fields.tags['en-US'];
              const newTags = tags.map(tag => findClosestTag(tag, validTags))
              entry.fields.tags['en-US'] = newTags
              console.log(tags, newTags)
              const updatedEntry = await entry.update()
              updatedEntry.publish().then(() => console.log('entry updated and published')).catch(()=>{failedRequests.push(entry.fields.slug)})
              await delay(150)
            }
          } catch {
            failedRequests.push(entry.fields.slug)
          }
        }
        console.log('completed script, failed entries: ')
        console.log(failedRequests)
      })
    })
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

function isChanged(entity) {
  return !!entity.sys.publishedVersion &&
      entity.sys.version >= entity.sys.publishedVersion + 2
}


function findClosestTag(input, stringArray) {
  let closestString = null;
  let minDistance = Infinity;

  for (const str of stringArray) {
    const distance = levenshtein(input, str);
    if (distance < minDistance) {
      minDistance = distance;
      closestString = str;
    }
  }

  return closestString;
}

fixBrokenTags({
  accessToken,
  spaceId,
  env: 'master',
}, page)
