const contentful = require('contentful-management')
const process = require('process');
const accessToken = process.argv[2] // CMA access token
const spaceId = process.argv[3] // Blog space id
const page = process.argv[4]

function publishAllDraftEntries(auth, page = 1) {

  const client = contentful.createClient({
    accessToken: auth.accessToken

  })

  client.getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env)).then(environment => {

      environment.getEntries({ limit: 500, skip: (page - 1) * 500, content_type: 'blogPost' }).then(async ({ items }) => {
        console.log(items.length)
        for (let entry of items) {
          if ((entry.isPublished() || isChanged(entry)) && !entry.fields.slug) {
            entry.fields.slug = { 'en-US': slugify(entry.fields.title['en-US']) }
            const updatedEntry = await entry.update()
            updatedEntry.publish().then(() => console.log('entry updated and published'))
            await delay(150)
          }
        }
      })
    })
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

function slugify(string) {
  return string.toLowerCase()
    .trim()
    .replace(/[^\w\s-]/g, '')
    .replace(/[\s_-]+/g, '-')
    .replace(/^-+|-+$/g, '');
}

function isChanged(entity) {
  return !!entity.sys.publishedVersion &&
    entity.sys.version >= entity.sys.publishedVersion + 2
}

publishAllDraftEntries({
  accessToken,
  spaceId,
  env: 'master',
}, page)
