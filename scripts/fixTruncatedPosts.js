const yaml = require('js-yaml');
const fs = require('fs');
const contentful = require('contentful-management')
const process = require('process');
const blogFiles = '/Users/mateofernandopenagos/Documents/gitlab/www-gitlab-com/sites/uncategorized/source/blog/blog-posts' // blog files folder in www to use as migration target
const accessToken = process.argv[2] // CMA access token
const spaceId = process.argv[3] // Blog space id


async function fixTruncatedPosts(auth) {

  const client = contentful.createClient({
    accessToken: auth.accessToken
  })
  const environment = await client.getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env))
  let total = 0
  let CmsEntries = []
  do {
    const response = await environment.getEntries({ content_type: 'blogPost', limit: 500, skip: CmsEntries.length });
    total = response.total;
    CmsEntries = CmsEntries.concat(response.items)
  } while (CmsEntries.length < total)
  let parsedPosts = parseBlogPostsToJSON()

  // console.log(CmsEntries[500].fields.body)
  // console.log(parsedPosts[500].markdown)
  const errorEntries = findTruncatedPosts(CmsEntries, parsedPosts)
  console.log(`updating ${errorEntries.length} entries`)
  for (let entry of errorEntries){
    console.log(`Updating ${entry.fields.slug['en-US']}`)
    entry.update().then(updatedEntry => {

      console.log(`${updatedEntry.fields.slug['en-US']} updated` )
    })
    await delay(150)

  }
}

function findTruncatedPosts(CmsEntries, parsedPosts) {
  const result = [];
  CmsEntries.forEach(entry => {
    const foundPost = parsedPosts.find(post => {
      if(post.canonical_path){
        const canonicalPathArray = post.canonical_path.split('/');
        const canonicalPath = canonicalPathArray[canonicalPathArray.length-1].length? canonicalPathArray[canonicalPathArray.length-1]: canonicalPathArray[canonicalPathArray.length-2];
        return canonicalPath === entry.fields.slug['en-US']
      } else{ 
        const [slugWithDate] = post.path.split('.')
        const [year, month, day, ...slugArray] = slugWithDate.split('-');
        const slug = slugArray.join('-');
        return slug === entry.fields.slug['en-US']
      }
    })
    if (foundPost && (foundPost.markdown.length !== entry.fields.body['en-US'].length)) {
      // console.log('===========================================================================================')
      // console.log(foundPost.markdown.length, 'POST MARKDOWN')
      // console.log(entry.fields.body['en-US'].length, 'ENTRY MARKDOWN')
      // console.log('===========================================================================================')
      if((entry.fields.body['en-US'].length/foundPost.markdown.length) <= 0.6){
        console.log('big length difference detected')
        console.log(entry.fields.slug['en-US'])
        entry.fields.body['en-US'] = foundPost.markdown
        result.push(entry)
      }

    }
  })
  return result
}

function parseBlogPostsToJSON() {
  console.log('parsing blog files to JSON')
  const blogPosts = fs.readdirSync(blogFiles);
  const parsedPosts = blogPosts.map(file => {
    const [__, yml, ...markdown] = fs.readFileSync(`${blogFiles}/${file}`, { encoding: 'utf8', flag: 'r' }).split('---')
    const parsedYML = yaml.load(yml, { json: true })
    // Make relative images absolute since the markdown references assets that are not in the CMS
    const updatedMarkdown = makeImagePathsAbsolute(markdown.join('---'))
    return { ...parsedYML, path: file, markdown: updatedMarkdown }
  }).filter(file => file)

  console.log(`Successfully parsed ${parsedPosts.length} blog files`)
  return parsedPosts;
}

function makeImagePathsAbsolute(markdownText) {
  let regex = /(!\[.*?\]\()(\/[^)]+)\)/g;

  let prefix = "https://about.gitlab.com";

  let newMarkdownText = markdownText.replace(regex, `$1${prefix}$2)`);


  return newMarkdownText
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}


fixTruncatedPosts({
  accessToken,
  spaceId,
  env: 'master'
})