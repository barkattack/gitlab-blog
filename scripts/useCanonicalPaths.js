const contentful = require('contentful-management')
const yaml = require('js-yaml');
const fs = require('fs');
const process = require('process');
const accessToken = process.argv[2] // CMA access token
const spaceId = process.argv[3] // Blog space id
const blogContentPath = process.argv[4] // Blog content path
const page = process.argv[5]

function publishAllDraftEntries(auth, page = 1) {

  const client = contentful.createClient({
    accessToken: auth.accessToken

  })

  client.getSpace(auth.spaceId)
    .then((space) => space.getEnvironment(auth.env)).then(environment => {

      const { parsedPosts } = parseToJSON(blogContentPath)
      environment.getEntries({ limit: 500, skip: (page - 1) * 500, content_type: 'blogPost' }).then(async ({ items }) => {
        console.log(items.length)
        for (let entry of items) {
          if ((entry.isPublished() || isChanged(entry))) {
            const existingPost = parsedPosts.find(post => post.title === entry.fields.title['en-US']);
            if(existingPost){
              if(existingPost.canonical_path){
                const canonicalPathArray = existingPost.canonical_path.split('/');
                const canonicalPath = canonicalPathArray[canonicalPathArray.length-1].length? canonicalPathArray[canonicalPathArray.length-1]: canonicalPathArray[canonicalPathArray.length-2]
                if(canonicalPath !== entry.fields.slug['en-US']){
                  entry.fields.slug = { 'en-US': canonicalPath }
                  const updatedEntry = await entry.update()
                  updatedEntry.publish().then(() => console.log(`${canonicalPath} entry updated and published with canonical path`))
                  await delay(150)
                } else{
                  console.log('Using correct canonical path')
                }
              } else {
                const [slugWithDate] = existingPost.path.split('.')
                const [year, month, day, ...slugArray] = slugWithDate.split('-');
                const slug = slugArray.join('-');
                if(slug !== entry.fields.slug['en-US']){
                  entry.fields.slug = { 'en-US': slug }
                  try {
                    const updatedEntry = await entry.update()
                    updatedEntry.publish().then(() => console.log(`${slug} entry updated and published with file slug`))
                    await delay(150)
                    
                  } catch (error) {
                    console.log('error: ', error)
                  }
                } else {
                  console.log('Using correct file title')
                }
              }
            }
          }
        }
      })
    })
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

function slugify(string) {
  return string.toLowerCase()
    .trim()
    .replace(/[^\w\s-]/g, '')
    .replace(/[\s_-]+/g, '-')
    .replace(/^-+|-+$/g, '');
}

function parseToJSON(blogContentPath) {
  console.log('parsing blog files to JSON')
  const blogPosts = fs.readdirSync(blogContentPath);
  const parsedPosts = blogPosts.map(file => {
    const [__, yml, ] = fs.readFileSync(`${blogContentPath}/${file}`, { encoding: 'utf8', flag: 'r' }).split('---')
    const parsedYML = yaml.load(yml, { json: true })
    // Make relative images absolute since the markdown references assets that are not in the CMS
    return { ...parsedYML, path: file }
  }).filter(file => file)

  console.log(`Successfully parsed ${parsedPosts.length} blog files`)

  return { parsedPosts };
}

function isChanged(entity) {
  return !!entity.sys.publishedVersion &&
    entity.sys.version >= entity.sys.publishedVersion + 2
}

publishAllDraftEntries({
  accessToken,
  spaceId,
  env: 'master',
}, page)


