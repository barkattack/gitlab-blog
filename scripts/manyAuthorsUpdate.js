const contentful = require('contentful-management')
const process = require('process');
const accessToken = process.argv[2] // CMA access token
const spaceId = process.argv[3] // Blog space id
const page = process.argv[4]

async function publishAllDraftEntries(auth, page = 1) {
  const client = contentful.createClient({
    accessToken: auth.accessToken
  })

  try {
    const space = await client.getSpace(auth.spaceId);
    const environment = await space.getEnvironment(auth.env);

    const { items } = await environment.getEntries({
      limit: 500,
      skip: (page - 1) * 500,
      content_type: 'blogPost'
    })
    console.log(items.length)
    for (let entry of items) {
      if ((entry.isPublished() || isChanged(entry)) && (entry.fields.previewSlug !== entry.fields.slug)) {
        entry.fields.authors = {
          'en-US': [{

            sys: {
              type: "Link",
              linkType: "Entry",
              id: entry.fields.author['en-US'].sys.id
            }
          }]
        }
      try {
        const updatedEntry = await entry.update();
        await updatedEntry.publish();
        console.log(`Entry ${entry.fields.title['en-US']} updated and published`);
        await delay(150);
      } catch (error) {
        console.error(
          `Error Updating Entry ${entry.fields.title['en-US']} 
                        
                         ${error}
                    ` )
      }
    }
  }
    } catch (error) {
  console.error(error);
}

console.log('Update finished')
process.exit(0)
}

function delay(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

function buildPreviewSlug(slug, dateString) {
  return `${dateString.replace(/-/g, '/')}/${slug}`;
}

function isChanged(entity) {
  return !!entity.sys.publishedVersion &&
    entity.sys.version >= entity.sys.publishedVersion + 2
}

(async () => {
  await publishAllDraftEntries({
    accessToken,
    spaceId,
    env: 'master',
  }, page)
})()


