const contentful = require('contentful-management')
const process = require('process');
const accessToken = process.argv[2] // CMA access token
const spaceId = process.argv[3] // Blog space id
const page = process.argv[4]

async function publishAllDraftEntries(auth, page = 1) {
    const client = contentful.createClient({
        accessToken: auth.accessToken
    })

    try {
        const space = await client.getSpace(auth.spaceId);
        const environment = await space.getEnvironment(auth.env);

        const {items} = await environment.getEntries({
            limit: 500,
            skip: (page - 1) * 500,
            content_type: 'blogPost'
        })
        console.log(items.length)
        for (let entry of items) {
            if ((entry.isPublished() || isChanged(entry)) && (entry.fields.previewSlug !== entry.fields.slug)) {
                entry.fields.previewSlug = {'en-US': buildPreviewSlug(entry.fields.slug['en-US'], entry.fields.date['en-US'])}
                try {
                    const updatedEntry = await entry.update();
                    await updatedEntry.publish();
                    console.log('Entry updated and published');
                    await delay(150);
                } catch (error) {
                    console.error(
                        `Error Updating Entry ${entry.fields.previewSlug['en-US']} 
                        
                         ${error}
                    ` )
                }
            }
        }
    } catch (error) {
        console.error(error);
    }

    console.log('Update finished')
    process.exit(0)
}

function delay(ms) {
    return new Promise((resolve) => {
        setTimeout(resolve, ms);
    });
}

function buildPreviewSlug(slug, dateString) {
    return `${dateString.replace(/-/g, '/')}/${slug}`;
}

function isChanged(entity) {
    return !!entity.sys.publishedVersion &&
        entity.sys.version >= entity.sys.publishedVersion + 2
}

(async () => {
    await publishAllDraftEntries({
        accessToken,
        spaceId,
        env: 'master',
    }, page)
})()


