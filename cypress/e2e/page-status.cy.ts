
describe('Page status check', () => {
    const baseUrl = Cypress.config('baseUrl') || 'https://about.gitlab.com';
  
    it('Should check for 404 on URLs from sitemap.xml', () => {
      const sitemapUrl = `${baseUrl}/blog/sitemap.xml`;
  
      cy.request(sitemapUrl).then((response) => {
        expect(response.status).to.eq(200);
  
        // Parse XML content
        const xmlContent = Cypress.$.parseXML(response.body);
        const urls = Cypress.$(xmlContent)
          .find('url loc')
          .map((index, element) => Cypress.$(element).text())
          .get();
  
        urls.forEach((url) => {
          if (url !== `${baseUrl}/404/`) {
            // Visit each URL and check for a 404 status
            cy.request({ url, failOnStatusCode: true });
          }
        });
      });
    });
  });