### Description
[Describe what feature you'd like to see]

### Goal
[Explain the objective of this feature]

### Designs
[Link to figma file or ux issue when applicable]

### Additional Information
- [Any other details or context you'd like to provide]

/label ~"dex-status::triage"
