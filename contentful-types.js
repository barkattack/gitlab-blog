export const CONTENT_TYPES = {
  BLOG_POST:"blogPost",
  BLOG_HERO: 'blogLandingPageHero',
  BLOG_CATEGORIES: 'category',
  BLOG_AUTHOR: 'author'
}

export const NAV_CONTENT_TYPES = {
  NAVIGATION: 'navigation',
  FOOTER: 'footer'
}