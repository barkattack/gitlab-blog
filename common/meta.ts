interface MetaDataParams {
  title?: string;
  description?: string;
  imageUrl?: string;
  url?: string;
  type?: string;
};

const defaults = {
  title: "Blog | GitLab",
  description: "Tutorials, product information, expert insights, and more from GitLab to help DevSecOps teams build, test, and deploy secure software faster.",
  imageUrl: "https://about.gitlab.com/nuxt-images/open-graph/open-graph-gitlab.png",
  url: "https://about.gitlab.com/blog/",
  type: 'website'
};

export const createMeta = ({ title = defaults.title, description = defaults.description, imageUrl, url = defaults.url, type = defaults.type }: MetaDataParams) => {
  return [
    { hid: 'og:title', property: 'og:title', content: title },
    { hid: 'twitter:title', property: 'twitter:title', content: title },
    { hid: 'description', name: 'description', content: description },
    { hid: 'og:description', property: 'og:description', content: description },
    { hid: 'twitter:description', property: 'twitter:description', content: description },
    { hid: 'og:image', property: 'og:image', content: imageUrl },
    { hid: 'twitter:image', property: 'twitter:image', content: imageUrl },
    { hid: 'og:url', property: 'og:url', content: url },
    { hid: 'og:type', property: 'og:type', content: type },
  ];
};

/*
  ------------------------------
  --------  schema.org  --------
  ------------------------------
*/
interface BlogPostingWrapper {
  json: Partial<BlogPosting>;
  type: string;
};

interface BlogPosting {
  "@context": string;
  "@type": string;
  mainEntityOfPage?: {
    "@type": string;
    "@id": string;
  };
  headline?: string;
  description?: string;
  image?: string;
  keywords?: string;
  articleSection?: string;
  author?: {
    "@type": string;
    name: string;
    url?: string;
  };
  publisher?: {
    "@type": string;
    name: string;
    logo: {
      "@type": string;
      url: string;
    };
  };
  datePublished?: string;
  timeRequired?: string;
};

export const createBlogPostSchema = ({
  mainEntityOfPageUrl,
  headline,
  description,
  imageUrl,
  keywords,
  articleSection,
  authorName,
  authorUrl,
  datePublished,
  timeRequired
}: {
  mainEntityOfPageUrl?: string,
  headline?: string,
  description?: string,
  imageUrl?: string,
  keywords?: string,
  articleSection?: string,
  authorName?: string,
  authorUrl?: string,
  datePublished?: string,
  timeRequired?: string,
} = {}): Partial<BlogPostingWrapper> => {
  const structuredData: Partial<BlogPosting> = {
    "@context": "https://schema.org",
    "@type": "BlogPosting",
  };

  if (mainEntityOfPageUrl) {
    structuredData.mainEntityOfPage = {
      "@type": "WebPage",
      "@id": mainEntityOfPageUrl
    };
  }

  if (headline) structuredData.headline = headline;
  if (description) structuredData.description = description;
  if (imageUrl) structuredData.image = imageUrl;
  if (keywords) structuredData.keywords = keywords;
  if (articleSection) structuredData.articleSection = articleSection;
  if (timeRequired) structuredData.timeRequired = timeRequired;

  if (authorName && authorUrl) {
    structuredData.author = {
      "@type": "Person",
      "name": authorName,
      "url": authorUrl
    };
  } else {
    structuredData.author = {
      "@type": "Organization",
      "name": "GitLab",
      "url": "https://about.gitlab.com/company/team/"
    };
  }

  structuredData.publisher = {
    "@type": "Organization",
    "name": "GitLab",
    "logo": {
      "@type": "ImageObject",
      "url": "https://about.gitlab.com/nuxt-images/open-graph/open-graph-gitlab.png"
    }
  };

  if (datePublished) structuredData.datePublished = datePublished;

  return {
    json: structuredData,
    type: 'application/ld+json'
  };
};

interface BreadcrumbWrapper {
  json: Partial<BreadcrumbList>;
  type: string;
};

interface BreadcrumbItem {
  "@type": string;
  position: number;
  name: string;
  item: string;
}

interface BreadcrumbList {
  "@context": string;
  "@type": string;
  itemListElement: BreadcrumbItem[];
}

export const createBreadcrumbSchema = (items: BreadcrumbItem[]): BreadcrumbWrapper => {
  const structuredData = {
    "@context": "https://schema.org/",
    "@type": "BreadcrumbList",
    "itemListElement": items
  };

  return {
    json: structuredData,
    type: 'application/ld+json'
  };
};
